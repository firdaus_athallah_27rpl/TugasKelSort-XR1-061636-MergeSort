package merge.sort;

public class MergeSort {

    public static void main(String[] args) {

        String identitas = "\n👦. Alif Rayhan Naufal Aziz \n👦. Firdaus Athallah Arya Putra Ardam \n👧. Rona Kamilia";
    System.out.println("Kelompok 06 :" + identitas);
    int i;
    int array[] = {6,5,3,1,8,7,2,4};
    
    System.out.print("\nNilai sebelum disorting:\n");
        for(i = 0; i < array.length; i++)
            System.out.print( array[i]+"  ");
            System.out.println();
    
    initializeMergeSort(array,0, array.length-1);
    System.out.print("setelah sorting:\n");
        for(i = 0; i <array.length; i++)
            System.out.print(array[i]+"  ");
            System.out.println();
    
    }

    public static void initializeMergeSort(int array[],int lo, int n){
 
    int low = lo;
    int high = n;
    if (low >= high) {
        return;
    }

    int middle = (low + high) / 2;
    initializeMergeSort(array, low, middle);
    initializeMergeSort(array, middle + 1, high);
    int end_low = middle;
    int start_high = middle + 1;
    while ((lo <= end_low) && (start_high <= high)) {
        if (array[low] < array[start_high]) {
            low++;
        } 
    
    else {
        int temp = array[start_high];
        for (int k = start_high- 1; k >= low; k--) {
            array[k+1] = array[k];
        }
                
            array[low] = temp;
            low++;
            end_low++;
            start_high++;

    }
    
}
    while ((lo <= end_low) && (start_high <= high)) {
        if (array[low] < array[start_high]) {
            low++;
        } 
    
    else {
        int temp = array[start_high];
        for (int k = start_high- 1; k >= low; k--) {
            array[k+1] = array[k];
        }
                
            array[low] = temp;
            low++;
            end_low++;
            start_high++;

    }
    
}
}
}